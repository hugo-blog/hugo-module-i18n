#!/bin/sh
if test -d node-i18n-iso-countries
then
	(cd node-i18n-iso-countries || exit; git pull)
else
	git clone https://github.com/michaelwittig/node-i18n-iso-countries.git
fi
rm data/*/country.yml 2>/dev/null
for file in node-i18n-iso-countries/langs/*
do
	base="${file##node-i18n-iso-countries/langs/}"
	lang="data/${base%%.json}"
	test -d "${lang}" || mkdir "${lang}"
	echo "---" > "${lang}/country.yml"
	jq \
		--sort-keys \
		'.countries|to_entries|map({"key":.key, "value":(.value | if type=="array" then .[0] else . end)})|from_entries' "${file}" \
	| ~/bin/yq e -P \
	>> "${lang}/country.yml"
done

# Hugo i18n module

This module provides some basic translations, partials, and shortcodes commonly
needed for multilingual websites:

* names of countries
* links to all language versions
* links to all other language versions (except the currently selected)
* link to the primary language version

Please note that this is not meant to be a generic collection. Everything that
is generic enough for widespread use to enable the functions of multilingual
websites could go here. This includes

* links to switch between languages
* date, time, number and currency handling
* display of the current language and appropriate symbols

Your specific translations should be kept directly in your websites.

We owe parts of these translations to the [congo theme](https://github.com/jpanther/congo) theme and other parts to the translation service [DeepL](https://www.deepl.com/translator).
